package cauldron.overlay;

public interface ICommandBehaviour {

    void run(String[] args);

}
