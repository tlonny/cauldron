package cauldron.graphics;

import org.joml.Vector2f;

public interface ITextureLookup {

    Vector2f[] getTextureOffsets();

}
